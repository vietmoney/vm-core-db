const Hoek = require('hoek');

class HelperAdapter {
  static extractColumnName(model) {
    Hoek.assert(model, '[Helper Adapter] model cannot null');
    let arrKeyModel = Object.keys(model),
      columnNames = [];
    arrKeyModel.forEach(e => {
      columnNames.push(`${model.tableAlias}.${HelperAdapter.toSnakeCase(e)}`);
    });
    return columnNames;
  }

  static extractColumnNameString(model, opts) {
    Hoek.assert(model, '[Helper Adapter] model cannot null');
    let columnNames = [],
      toStringColumn = (e, tableAlias) => {
        return `${tableAlias}.${HelperAdapter.toSnakeCase(
          e
        )} AS ${tableAlias}_${HelperAdapter.toSnakeCase(e)}`;
      },
      handleColumnModel = (model, tableAlias) => {
        let arrKeyModel = Object.keys(model);
        arrKeyModel.forEach(e => {
          columnNames.push(toStringColumn(e, tableAlias || model.tableAlias));
        });
      };

    if (!model.length) handleColumnModel(model);
    else if (model.length > 0)
      model.forEach(e => {
        if (e.modelClass) handleColumnModel(e.modelClass, e.tableAlias);
        else handleColumnModel(e);
      });

    return columnNames;
  }

  static toSnakeCase(word, opts) {
    let delimiter = '_';
    if (opts && opts.delimiter) {
      delimiter = opts.delimiter;
    }
    const reg = new RegExp(`^${delimiter}`);
    return word
      .replace(/(?:^|\.?)([A-Z])/g, (x, y) => {
        return `${delimiter}${y.toLowerCase()}`;
      })
      .replace(reg, '');
  }

  static toCamelCase(word) {
    return word.replace(/(?:^|\.?)(_[a-z])/g, (x, y) => {
      return y.replace('_', '').toUpperCase();
    });
  }

  static filterParams(params, model, opts) {
    let columns = Object.keys(model),
      wheres = [],
      args = [],
      ors = [];
    const tableAlias = opts.tableAlias || model.tableAlias,
      parseObjectQuery = arg => {
        let typeWhere, argNeedPush;
        const keyArg = Object.keys(arg);
        if (keyArg.includes('like')) {
          typeWhere = 'LIKE ?';
          argNeedPush = `%${arg.like}%`;
        } else if (keyArg.includes('from') && keyArg.includes('to')) {
          typeWhere = 'BETWEEN ? AND ?';
          argNeedPush = [arg.from, arg.to];
        } else if (keyArg.includes('in')) {
          if (Array.isArray(arg.in) && arg.in.length > 0) {
            typeWhere = 'IN (?)';
            argNeedPush = arg.in;
          } else {
            Hoek.assert(
              false,
              `Query IN length array is equal 0, you need handle this. Info ${arg.toString()}`
            );
          }
        } else if (keyArg.includes('text')) {
          typeWhere = '= ?';
          argNeedPush = arg.text.toString();
        } else if (keyArg.includes('empty')) {
          typeWhere = 'IS NULL';
        } else if (keyArg.includes('isNot')) {
          typeWhere = '= ?';
          argNeedPush = arg.isNot.toString();
        }
        return {
          argNeedPush: argNeedPush,
          typeWhere: typeWhere
        };
      };

    if (typeof params === 'object') {
      for (let key of Object.keys(params)) {
        let arg = params[key],
          not = false,
          checkSpecial = false,
          operator = '=',
          keyDate = false,
          pushWhere = true;
        if (columns.indexOf(key) >= 0 || key === 'or') {
          if (key === 'or') {
            params.or.forEach(e => {
              let keys = Object.keys(e);
              let key = keys[0];
              let value = e[key];
              const columnNameFlowKey = `${
                keyDate ? 'DATE(' : ''
              }${tableAlias}.${HelperAdapter.toSnakeCase(key)}${keyDate ? ')' : ''}`;
              ors.push({
                where: `${columnNameFlowKey} LIKE ?`,
                arg: `%${value}%`
              });
            });
          } else {
            delete params.or;
            if (typeof arg === 'string') {
              if (/^NOT/g.test(arg)) {
                arg = arg.replace('NOT', '').trim();
                not = true;
              }
              if (/LIKE/g.test(arg)) {
                operator = 'ALL';
              }
              if (/IN/g.test(arg)) {
                operator = 'ALL';
              }
              if (/BETWEEN/g.test(arg)) {
                operator = 'ALL';
              }
              if (/IS NULL/g.test(arg)) {
                operator = 'ALL';
              }
              if (/FULL/g.test(arg)) {
                checkSpecial = true;
                arg = arg.replace('FULL', '').trim();
              }

              if (operator !== 'ALL' && !checkSpecial) {
                if (/[^\w\s-]/gi.test(arg)) {
                  operator = /[^\w\s-]/gi.exec(arg);
                  arg = arg.replace(/[^\w\s-]/gi, '').trim();
                }
              }
            } else if (typeof arg === 'object') {
              const propertyArg = parseObjectQuery(arg),
                typeWhere = propertyArg.typeWhere,
                argNeedPush = propertyArg.argNeedPush,
                keyArg = Object.keys(arg);

              if (typeWhere && argNeedPush) {
                if (keyArg.includes('from') && keyArg.includes('to'))
                  argNeedPush.forEach(e => {
                    args.push(e);
                  });
                else args.push(argNeedPush);
                if (keyArg.includes('isNot')) {
                  not = true;
                }
              } else {
                if (keyArg.includes('or')) {
                  pushWhere = false;
                }
              }
              if (keyArg.includes('typeDate')) {
                keyDate = true;
              }

              arg = typeWhere ? `${arg.not ? 'NOT ' : ''}${typeWhere}` : '';
              if (operator === '=') operator = 'ALL';
            }

            if (operator !== 'ALL') {
              args.push(arg);
            }
            const columnNameFlowKey = `${
              keyDate ? 'DATE(' : ''
            }${tableAlias}.${HelperAdapter.toSnakeCase(key)}${keyDate ? ')' : ''}`;
            if (pushWhere) {
              wheres.push(
                `${not ? 'NOT' : ''} ${columnNameFlowKey} ${
                  operator === 'ALL' ? arg : `${operator} ?`
                }`
              );
            } else {
              params.or.forEach(e => {
                const propertyArg = parseObjectQuery(e);
                let not;
                if (e.isNot) {
                  not = true;
                }
                ors.push({
                  where: `${not ? ' NOT ' : ''}${columnNameFlowKey} ${propertyArg.typeWhere}`,
                  arg: propertyArg.argNeedPush
                });
              });
            }
          }
        } else {
          Hoek.assert(false, `Filter params key ${key} of ${model.tableName} not exist`);
          break;
        }
      }
      return {
        where: wheres,
        args: args,
        ors: ors
      };
    } else {
      Hoek.assert(
        false,
        '[Helper Adapter] function filterParams detect class model can not exist columns or params where not object'
      );
    }
  }

  static sqlOrder(model, value, opts) {
    opts = opts || {};
    value = value || {};

    let order = '',
      includes = ['create_at', 'id', 'uid'],
      valueOrder = value.order,
      getStrOrder = src => {
        let str;
        if (src.indexOf('-') >= 0) str = 'DESC';
        else str = 'ASC';
        return str;
      },
      getFullColumns = (key, store) => {
        let tableAlias = model.tableAlias;
        if (store) {
          tableAlias = store.model.tableAlias;
        }
        return `${tableAlias}.${key}`;
      },
      orderString = store => {
        let key;
        if (store) {
          key = HelperAdapter.toSnakeCase(valueOrder.by.replace('-', '').trim());
          order = `${getFullColumns(key, store)} ${getStrOrder(valueOrder.by)}`;
        } else {
          key = HelperAdapter.toSnakeCase(valueOrder.replace('-', '').trim());
          order = `${getFullColumns(key)} ${getStrOrder(valueOrder)}`;
        }
      },
      orderArray = () => {
        order = [];
        valueOrder.forEach(e => {
          if (e && e.storeModel) {
            order.push(
              `${getFullColumns(
                HelperAdapter.toSnakeCase(e.by.replace('-', '').trim()),
                e.storeModel
              )} ${getStrOrder(e.by)}`
            );
          } else {
            order.push(
              `${getFullColumns(
                HelperAdapter.toSnakeCase(e.replace('-', '').trim())
              )} ${getStrOrder(e)}`
            );
          }
        });
      };

    if (value ? valueOrder : false) {
      if (typeof valueOrder === 'string') {
        orderString();
      } else if (valueOrder.length ? valueOrder.length > 0 : false) {
        orderArray();
      } else if (typeof valueOrder === 'object') {
        Hoek.assert(valueOrder.storeModelOrder, 'Cannot empty function sqlOrder helper adapter');
        if (typeof valueOrder.by === 'string') {
          orderString(valueOrder.storeModelOrder);
        }
      }
    } else {
      let columnsDefault;
      for (let e of includes) {
        if (Object.keys(model).indexOf(e) >= 0) {
          columnsDefault = getFullColumns(e);
          break;
        }
      }
      if (!columnsDefault) {
        columnsDefault = getFullColumns(HelperAdapter.toSnakeCase(Object.keys(model)[0]));
      }

      order = `${columnsDefault} DESC`;
    }

    if (opts && opts.extendOrder) {
      let stringOrderExtend = [];
      if (!Array.isArray(opts.extendOrder)) {
        opts.extendOrder = [opts.extendOrder];
      }
      opts.extendOrder.forEach(e => {
        stringOrderExtend.push(
          `${HelperAdapter.toSnakeCase(e.replace('-', '').trim())} ${getStrOrder(e)}`
        );
      });
      stringOrderExtend = stringOrderExtend.join(',');
      if (Array.isArray(order)) {
        if (opts.positionOrderExtend) {
          order.splice(opts.positionOrderExtend, 0, stringOrderExtend);
        } else {
          order.push(stringOrderExtend);
        }
      } else {
        if (order.length > 0) {
          order = `${stringOrderExtend},${order}`;
        } else {
          order = stringOrderExtend;
        }
      }
    }

    return ` ORDER BY ${Array.isArray(order) ? order.join(', ') : order}`;
  }

  static sqlRelation(model, selectOptions, opts) {
    let includes = selectOptions.includes,
      sqlJoin = [],
      sqlJoinWhere = [],
      extractSqlJoin = e => {
        let join = '',
          sql = src => {
            let str = '',
              tableJoin = src.storeModel.model,
              tableAliasBase = '',
              typeJoinStr = `${src.typeJoin} JOIN `;
            tableAliasBase = model.tableAlias;
            const tableJoiAlias = src.tableAlias || tableJoin.tableAlias;
            str = `${typeJoinStr} ${
              tableJoin.tableName
            } ${tableJoiAlias} ON ${tableJoiAlias}.${HelperAdapter.toSnakeCase(
              src.columns[0]
            )} = ${tableAliasBase}.${HelperAdapter.toSnakeCase(src.columns[1])}`;
            return str;
          };

        switch (e.typeJoin) {
          case 'INNER':
            break;
          case 'LEFT':
            break;
          case 'RIGHT':
            break;
          default:
            e.typeJoin = 'LEFT';
        }

        if (e.storeModel) join = sql(e);

        /**
         * object JoinMany
         * {joinMany:[{columns:['foo','foo2'],storeModel:model,where:{filter}}]}
         *
         return sqlJoin
         */
        const extractSqlJoinItem = (src, modelPrevious) => {
            const typeJoinStr = `${src.typeJoin} JOIN `,
              table = modelPrevious || model,
              tableJoin = src.storeModel.model,
              tableJoiAlias = src.tableAlias || tableJoin.tableAlias;

            return `${typeJoinStr} ${
              tableJoin.tableName
            } ${tableJoiAlias} ON ${tableJoiAlias}.${HelperAdapter.toSnakeCase(src.columns[0])} = ${
              table.tableAlias
            }.${HelperAdapter.toSnakeCase(src.columns[1])}`;
          },
          joinMany = e.joinMany;

        let sqlJoinItem = [];

        if (joinMany && joinMany.length > 0)
          e.joinMany.forEach((join, index) => {
            join.typeJoin = join.typeJoin || e.typeJoin;
            if (index > 0) {
              sqlJoinItem.push(extractSqlJoinItem(join, joinMany[index - 1].storeModel.model));
            } else {
              sqlJoinItem.push(extractSqlJoinItem(join));
            }
          });

        if (sqlJoinItem.length > 0) return sqlJoinItem.join(' ');
        return join;
      };

    if (selectOptions ? includes.length > 0 : false) {
      includes.forEach(e => {
        if (opts.functionCheckRelation.checkRelationType(e)) {
          if (e.joinMany && e.joinMany.length > 0)
            e.joinMany.forEach(join => {
              if (join.where)
                sqlJoinWhere.push(
                  HelperAdapter.filterParams(join.where, join.storeModel.model, {
                    tableAlias: join.tableAlias
                  })
                );
            });
          if (e.where)
            sqlJoinWhere.push(
              HelperAdapter.filterParams(e.where, e.storeModel.model, {
                tableAlias: e.tableAlias
              })
            );

          sqlJoin.push(extractSqlJoin(e));
        }
      });

      return {
        sqlJoin: sqlJoin,
        sqlJoinWhere: sqlJoinWhere
      };
    }
  }

  static sqlLimit(selectOptions, opts) {
    let limit = selectOptions ? (selectOptions.limit ? selectOptions.limit : 1) : 1;
    if (limit ? isNaN(limit) : false) {
      limit = 1;
    }
    if (selectOptions ? selectOptions.getMany : false) {
      return '';
    }

    return ` LIMIT ${limit} `;
  }

  static extractSqlWhere(one, two) {
    if (!two) return one.join(' AND ');
    return `${
      one.length > 0 ? (two.length > 0 ? `${one.join(' AND ')} AND` : one.join(' AND ')) : ''
    }`;
  }

  static sqlPagination(params, opts) {
    let paging = '',
      pageNumber = 1,
      pageIndex = 0;
    opts = opts || {};

    let emptyFunc = () => {
      return {
        sql: false,
        pageNumber: pageNumber,
        pageIndex: pageIndex
      };
    };

    if (!params) {
      return emptyFunc();
    }

    if (params.paging) {
      params = params.paging;
    }

    if (params.pageNumber) {
      pageNumber = params.pageNumber < 1 ? 1 : params.pageNumber;
    }

    if (params.pageSize ? params.pageSize <= 0 : true) {
      return emptyFunc();
    }

    if (pageNumber === 1) {
      paging = ` LIMIT  ${params.pageSize} `;
    } else {
      pageIndex = (pageNumber - 1) * params.pageSize;
      paging = ` LIMIT ${params.pageSize} OFFSET ${pageIndex} `;
    }

    return {
      sql: paging,
      pageNumber: pageNumber,
      pageIndex: pageIndex
    };
  }

  static formData(data, model, opts) {
    opts = opts || {};
    Hoek.assert(model, 'Function formData not found model');
    if (model.formData) {
      data = model.formData(data, opts);
      return data;
    }
    return data;
  }

  static responseData(data, model) {
    Hoek.assert(model, 'Function formData not found model');
    if (model.responseData) {
      data = model.responseData(data);
      return data;
    }
    return data;
  }
}

module.exports = HelperAdapter;
