const config = require('config');
const mysql = require('mysql');
const Hoek = require('hoek');
const log = require('core-express').Log;
const BPromise = require('bluebird');

class Pool {

  get pool() {
    return mysql.createPool(config.mysql);
  }

  run() {
    return new BPromise((resolve, reject) => {
      if (config.mysql) {
        let pool = this.pool;
        pool.getConnection((err, connection) => {
          if (err) {
            log.error(err);
            log.error('Create pool error mysql');
            return reject(err);
          }
          log.info('Create pool successfully mysql');
          log.info({
            mysql: {
              host: config.mysql.host,
              port: config.mysql.port || 3306
            }
          });
          return resolve(pool);
        });

      } else {
        Hoek.assert(config.mysql, '[Database] environment is not config database');
      }
    });

  }
}

const PoolClass = new Pool();
PoolClass.run();
module.exports = PoolClass.pool;